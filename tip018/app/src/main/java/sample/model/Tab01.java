package sample.model;

// import jakarta.annotation.Generated;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class Tab01 {

    private Short num01;
    private String chr01;

    // @JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss")
    private Date dat01;

}
