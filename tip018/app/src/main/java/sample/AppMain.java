// AppMain.java
package sample;

import static org.mybatis.dynamic.sql.SqlBuilder.*;
import static sample.mapper.Tab01DynamicSqlSupport.*;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.text.SimpleDateFormat;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.fasterxml.jackson.databind.SequenceWriter;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import sample.mapper.Tab01Mapper;
import sample.model.Tab01;

public class AppMain {

    public static void main(String[] args) throws IOException {
        new AppMain().doMain();
    }

    public void doMain() throws IOException{
        Reader r = Resources.getResourceAsReader("mybatis-config.xml");
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(r);
        // factory.getConfiguration().addMapper(HogeMapper.class);
        SqlSession sess = factory.openSession();
        
        Tab01Mapper mapper = sess.getMapper(Tab01Mapper.class);
        List<Tab01> list = mapper.select(c -> 
            c.where(num01, isEqualTo(Short.valueOf("1")))
             .and(chr01, isEqualTo("abc"))
        );
        list.forEach(e->System.out.printf("** %s, %s, %s.\n", e.getNum01(), e.getChr01(), e.getDat01()));

        CsvSchema schema = CsvSchema .builder()
            .addColumns(List.of("num01", "chr01", "dat01"), CsvSchema.ColumnType.STRING)  // 対象フィールドの指定
            .setUseHeader(true)         // print header
            .setColumnSeparator(',')    // default
            .setQuoteChar('"')          // default
            .build()
        ;

        BufferedWriter writer = Files.newBufferedWriter(Paths.get("books_from_array.csv"), StandardCharsets.UTF_8);
        CsvMapper csvMapper = new CsvMapper();
        SequenceWriter sequenceWriter = csvMapper
            .setDateFormat(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss"))
            .writerFor(Tab01.class)
            .with(schema)
            .writeValues(writer)
        ;
        list.forEach(e->{
            try {
                sequenceWriter.write(e);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });

    }
}

