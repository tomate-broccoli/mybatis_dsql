// AppMain.java
package sample;

import static org.mybatis.dynamic.sql.SqlBuilder.*;
import static sample.mapper.Tab01DynamicSqlSupport.*;

import java.io.IOException;
import java.io.Reader;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import sample.mapper.Tab01Mapper;
import sample.model.Tab01;

public class AppMain {

    public static void main(String[] args) throws IOException {
        new AppMain().doMain();
    }

    public void doMain() throws IOException{
        Reader r = Resources.getResourceAsReader("mybatis-config.xml");
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(r);
        // factory.getConfiguration().addMapper(HogeMapper.class);
        SqlSession sess = factory.openSession();
        
        Tab01Mapper mapper = sess.getMapper(Tab01Mapper.class);
        List<Tab01> list = mapper.select(c -> 
            c.where(num01, isEqualTo(Short.valueOf("1")))
             .and(chr01, isEqualTo("abc"))
        );
        list.forEach(e->System.out.printf("** %s, %s, %s.\n", e.getNum01(), e.getChr01(), e.getDat01()));
    }
}

