package sample.mapper;

import static sample.mapper.Tab01DynamicSqlSupport.*;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.dynamic.sql.BasicColumn;
import org.mybatis.dynamic.sql.select.SelectDSLCompleter;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
import org.mybatis.dynamic.sql.util.mybatis3.CommonCountMapper;
import org.mybatis.dynamic.sql.util.mybatis3.CommonDeleteMapper;
import org.mybatis.dynamic.sql.util.mybatis3.CommonInsertMapper;
import org.mybatis.dynamic.sql.util.mybatis3.CommonUpdateMapper;
import org.mybatis.dynamic.sql.util.mybatis3.MyBatis3Utils;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;

// import sample.mapper.Tab01DynamicSqlSupport.Tab01;
import sample.model.Tab01;

@Mapper
public interface Tab01Mapper extends CommonCountMapper, CommonDeleteMapper, CommonInsertMapper, CommonUpdateMapper {

    BasicColumn[] selectList = BasicColumn.columnList(num01, chr01, dat01);

    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    @Results(id="MTab01Result", value = {
        @Result(column="NUM01", property="num01", jdbcType=JdbcType.NUMERIC),
        @Result(column="CHR01", property="chr01", jdbcType=JdbcType.VARCHAR),
        @Result(column="DAT01", property="dat01", jdbcType=JdbcType.TIMESTAMP)
    })
    List<Tab01> selectMany(SelectStatementProvider selectStatement);

    default List<Tab01> select(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectList(this::selectMany, selectList, Tab01, completer);
    }

}