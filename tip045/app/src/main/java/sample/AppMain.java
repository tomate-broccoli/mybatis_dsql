package sample;

import static org.mybatis.dynamic.sql.SqlBuilder.*;
import static sample.mapper.Tab01DynamicSqlSupport.*;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.mybatis.dynamic.sql.select.QueryExpressionDSL.QueryExpressionWhereBuilder;
import org.mybatis.dynamic.sql.AndOrCriteriaGroup;

import java.io.Reader;
import java.util.List;
import java.util.ArrayList;

import sample.mapper.Tab01Mapper;
import sample.model.Tab01;

public class AppMain {

    public static void main(String[] args) throws Exception {
        new AppMain().doMain();
    }

    public void doMain() throws Exception{
        // セッションの取得
        Reader r = Resources.getResourceAsReader("mybatis-config.xml");
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(r);
        SqlSession sess = factory.openSession();

        // マッパの取得
        Tab01Mapper mapper = sess.getMapper(Tab01Mapper.class);

        // 一覧の取得
        List<Tab01> list = mapper.select(dsl ->{ 
            QueryExpressionWhereBuilder builder = dsl.where();

            List<AndOrCriteriaGroup> cond1= new ArrayList();
            cond1.add(and(num01, isGreaterThanOrEqualToWhenPresent(Short.valueOf("1"))));
            builder.or(cond1);

            return builder;
        });
        list.forEach(e->System.out.printf("** %s, %s, %s.\n", e.getNum01(), e.getChr01(), e.getDat01()));

    }

}
