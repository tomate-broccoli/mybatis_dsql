package sample;

import static org.mybatis.dynamic.sql.SqlBuilder.*;
import static sample.mapper.Tab01DynamicSqlSupport.*;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.BufferedWriter;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.text.SimpleDateFormat;

import sample.mapper.Tab01Mapper;
import sample.model.Tab01;

public class AppMain {

    public static void main(String[] args) throws Exception {
        new AppMain().doMain();
    }

    public void doMain() throws Exception{
        Optional<Class<Short>> opt = num01.javaType();
        System.out.println(opt.isPresent());
//        Class<Short> cls = num01.javaType().get();
        Class<Short> cls = opt.get();
        System.out.println(cls.getName());
        System.out.println(cls.getSimpleName());
        System.out.println(cls.getTypeName());
        System.out.println(cls.getCanonicalName());
    }

    public void doMain2() throws Exception{
        // セッションの取得
        Reader r = Resources.getResourceAsReader("mybatis-config.xml");
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(r);
        SqlSession sess = factory.openSession();

        // マッパの取得
        Tab01Mapper mapper = sess.getMapper(Tab01Mapper.class);

        // 一覧の取得
        List<Tab01> list = mapper.select(c -> 
            c.where(num01, isEqualTo(Short.valueOf("1")))
             .and(chr01, isEqualTo("abc"))
        );
        list.forEach(e->System.out.printf("** %s, %s, %s.\n", e.getNum01(), e.getChr01(), e.getDat01()));

    }
}
